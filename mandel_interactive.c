#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <sys/types.h>
//#include <OpenCL/opencl.h>
#include <unistd.h>
#include <CL/cl.h>
#include <sys/resource.h>
#include <sys/time.h>

#define RUN_SERIAL     0
#define RUN_OPENCL_CPU 1
#define RUN_OPENCL_GPU 2
#define RUN_OPENCL_HETEROGENEOUS 3
int run_mode;

void set_texture();

typedef struct {
	unsigned char r, g, b;
} rgb_t;
rgb_t **tex = 0;
int gwin;
GLuint texture;
int width, height;
int tex_w, tex_h;
double scale = 1. / 256;
double cx = -.6, cy = 0;
int color_rotate = 0;
int saturation = 1;
int invert = 0;
int max_iter = 256;
int sizeTex=0;


cl_context contextCPU;       // compute context	
cl_command_queue command_queueCPU;
cl_program programCPU;       // compute program
cl_kernel kernelCPU;
cl_device_id device_idCPU;     // compute device id


cl_context contextGPU;       // compute context	
cl_command_queue command_queueGPU;
cl_program programGPU;       // compute program
cl_kernel kernelGPU;
cl_device_id device_idGPU;     // compute device id

/* From common.c */
extern double getMicroSeconds();
extern char *err_code (cl_int err_in);
extern int output_device_info(cl_device_id device_id);
extern void errNDRANge(int err);



/* Time */
#include <sys/time.h>
#include <sys/resource.h>

void inicializaGPU(cl_context *context ,cl_command_queue * command_queue, cl_program *program,cl_kernel *kernel, int gpucpu,cl_device_id *device_id){

	cl_int err;

	FILE* programHandle;
    size_t programSize, kernelSourceSize;
    char *programBuffer, *kernelSource;

 // get size of kernel source
    programHandle = fopen("mandelKernel.cl", "r");
    fseek(programHandle, 0, SEEK_END);
    programSize = ftell(programHandle);
    rewind(programHandle);

 // read kernel source into buffer
    programBuffer = (char*) malloc(programSize + 1);
    programBuffer[programSize] = '\0';
    int p =  fread(programBuffer, sizeof(char), programSize, programHandle);   
    fclose(programHandle);


	    // Set up platform and GPU device

	cl_uint numPlatforms= 0;
	//printf("Ya hemos leido el kernel del fichero%i\n",2);
	// Find number of platforms	
	

	
	err = clGetPlatformIDs(0, NULL, &numPlatforms);
	if (err != CL_SUCCESS || numPlatforms <= 0)
	{
		printf("Error: Failed to find a platform!\n%s\n",err_code(err));
		exit(1);
	}	
	//printf("Plataformas obtenidas!%i\n",1);

	// Get all platforms
	cl_platform_id Platform[numPlatforms];
	err = clGetPlatformIDs(numPlatforms, Platform, NULL);
	if (err != CL_SUCCESS || numPlatforms <= 0)
	{
		printf("Error: Failed to get the platform!\n%s\n",err_code(err));
		exit(1);
	}
	//printf("Id de Plataformas obtenidas!\n");

	// Secure a GPU
	if (gpucpu==0){

		int i;
		for (i = 0; i < numPlatforms; i++)
		{
			err = clGetDeviceIDs(Platform[i], CL_DEVICE_TYPE_CPU, 1, device_id, NULL);
			if (err == CL_SUCCESS)
			{
				break;
			}
		}
	}
	else{

		int i;
		for (i = 0; i < numPlatforms; i++)
		{
			err = clGetDeviceIDs(Platform[i], CL_DEVICE_TYPE_GPU, 1, device_id, NULL);
			if (err == CL_SUCCESS)
			{
				break;
			}
		}

	}

	//printf("CPU asegurada!\n");

	if (*device_id == NULL)
	{
		printf("Error: Failed to create a device group!\n%s\n",err_code(err)); 
		exit(1);
	}

	

	// Create a compute context 
	*context = clCreateContext(0, 1, device_id, NULL, NULL, &err);	
	if (err!= CL_SUCCESS)
	{
		printf("Error: Failed to create a compute context!\n%s\n", err_code(err));
		exit(1);
	}
	//printf("Contexto listo\n"); 
	// create command queue 
	*command_queue = clCreateCommandQueue(*context,*device_id, 0, &err);
	if (err != CL_SUCCESS)
	{	
		printf("Unable to create command queue. Error Code=%s\n",err_code(err));
		exit(1);
	}
	 
	// create program object from source. 
	// kernel_src contains source read from file earlier
	//printf("Comandos listos\n");
	*program = clCreateProgramWithSource(*context, 1 ,(const char**) &programBuffer, NULL, &err);
	if (err != CL_SUCCESS)
	{	
		printf("Unable to create program object. Error Code=%s\n",err_code(err));
		exit(1);
	}  

	/**program = clCreateProgramWithSource(*context, 1, (const char **) &KernelSource, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n%s\n", err_code(err));
        exit(1);
    } */ 
    const char options[] = "-cl-std=CL1.1 -cl-mad-enable -Werror";	
	err = clBuildProgram(*program, 1, device_id, options, NULL, NULL);	

	if (err != CL_SUCCESS)
	{
        	printf("Build failed. Error Code=%s\n", err_code(err));

		size_t len;
		char buffer[2048];
		// get the build log
		clGetProgramBuildInfo(*program, *device_id, CL_PROGRAM_BUILD_LOG,
                                  sizeof(buffer), buffer, &len);
		printf("--- Build Log -- \n %s\n",buffer);
		exit(1);
	}
	//printf("Programa listo\n");
	

	*kernel = clCreateKernel(*program, "mandel", &err);
	if (err != CL_SUCCESS)
	{	
		printf("Unable to create kernel object. Error Code=%s\n",err_code(err));
		exit(1);
	}

}

double calc_mandel_opencl_Heterogeneous(){
	cl_context contextCPU_h;       // compute context	
	cl_command_queue command_queueCPU_h;
	cl_program programCPU_h;       // compute program
	cl_kernel kernelCPU_h;
	cl_device_id device_idCPU_h;     // compute device id


	cl_context contextGPU_h;       // compute context	
	cl_command_queue command_queueGPU_h;
	cl_program programGPU_h;       // compute program
	cl_kernel kernelGPU_h;
	cl_device_id device_idGPU_h;     // compute device id
	double t0;
	int err;
	
	//inicializamos la GPU y la CPU simultaneamente
	inicializaGPU(&contextCPU_h,&command_queueCPU_h,&programCPU_h,&kernelCPU_h,0,&device_idCPU_h);
	inicializaGPU(&contextGPU_h,&command_queueGPU_h,&programGPU_h,&kernelGPU_h,1,&device_idGPU_h);
	t0 = getMicroSeconds();	

	//usaremos un 20% del tex en la CPU y el otro 80% en la GPU
	cl_mem d_tex_GPU;  
	cl_mem d_tex_CPU;
	size_t globalCPU[2];   
	size_t globalGPU[2];   
	int tamCPU = ceil(sizeTex*0.2);
	int tamGPU = ceil(sizeTex*0.8);

	
	
	output_device_info(device_idCPU_h);
	output_device_info(device_idGPU_h);
	
	d_tex_CPU=clCreateBuffer(contextCPU_h,  CL_MEM_READ_WRITE, tamCPU, NULL, &err);
	errNDRANge(err);
	d_tex_GPU=clCreateBuffer(contextGPU_h,  CL_MEM_READ_WRITE, tamGPU, NULL, &err);
	errNDRANge(err);
	
	
	errNDRANge(clEnqueueWriteBuffer(command_queueCPU_h, d_tex_CPU, CL_TRUE, 0, tamCPU,tex[0], 0, NULL, NULL));	
	clEnqueueWriteBuffer(command_queueGPU_h, d_tex_GPU, CL_TRUE,0, tamGPU,tex[0]+tamCPU, 0, NULL, NULL);

	

	
	int heightCPU = ceil(height*0.2);
	int heightGPU = ceil(height*0.8);
	

	
	
	if (clSetKernelArg(kernelCPU_h, 0, sizeof(cl_int), &heightCPU) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernelCPU_h, 1, sizeof(cl_int), &width)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 1\n");
	if (clSetKernelArg(kernelCPU_h, 2, sizeof(cl_double), &scale)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 2\n");
	if (clSetKernelArg(kernelCPU_h, 3, sizeof(cl_double), &cx)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 3\n");
	if (clSetKernelArg(kernelCPU_h, 4, sizeof(cl_double), &cy)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 4\n");
	if (clSetKernelArg(kernelCPU_h, 5, sizeof(cl_int), &max_iter)!= CL_SUCCESS)		
		printf("a fallado el setKernelArg 5\n");		
	if (clSetKernelArg(kernelCPU_h, 6, sizeof(cl_mem), &d_tex_CPU)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 6\n");
	if (clSetKernelArg(kernelCPU_h, 7, sizeof(cl_int), &invert)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 7\n");
	if (clSetKernelArg(kernelCPU_h, 8, sizeof(cl_int), &saturation)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 8\n");
	if (clSetKernelArg(kernelCPU_h, 9, sizeof(cl_int), &color_rotate)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 9\n");



	if (clSetKernelArg(kernelGPU_h, 0, sizeof(cl_int), &heightGPU) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernelGPU_h, 1, sizeof(cl_int), &width)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 1\n");
	if (clSetKernelArg(kernelGPU_h, 2, sizeof(cl_double), &scale)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 2\n");
	if (clSetKernelArg(kernelGPU_h, 3, sizeof(cl_double), &cx)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 3\n");
	if (clSetKernelArg(kernelGPU_h, 4, sizeof(cl_double), &cy)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 4\n");
	if (clSetKernelArg(kernelGPU_h, 5, sizeof(cl_int), &max_iter)!= CL_SUCCESS)		
		printf("a fallado el setKernelArg 5\n");		
	if (clSetKernelArg(kernelGPU_h, 6, sizeof(cl_mem), &d_tex_GPU)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 6\n");
	if (clSetKernelArg(kernelGPU_h, 7, sizeof(cl_int), &invert)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 7\n");
	if (clSetKernelArg(kernelGPU_h, 8, sizeof(cl_int), &saturation)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 8\n");
	if (clSetKernelArg(kernelGPU_h, 9, sizeof(cl_int), &color_rotate)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 9\n");
	


	// set the global work dimension size
	globalCPU[0] = heightCPU;
	globalCPU[1] = width;

	globalGPU[0] = heightGPU;
	globalGPU[1] = width;

	
	
	err = clEnqueueNDRangeKernel(command_queueCPU_h, kernelCPU_h, 2, NULL, globalCPU, NULL,0, NULL, NULL);	

	if (err != CL_SUCCESS) {
		printf("Unable to enqueue kernel command. Error Code=%d\n", err);
		exit(1);
	}
	// wait for the command to finish
	clFinish(command_queueCPU_h);

	
	err = clEnqueueNDRangeKernel(command_queueGPU_h, kernelGPU_h, 2, NULL, globalGPU, NULL,0, NULL, NULL);	

	if (err != CL_SUCCESS) {
		printf("Unable to enqueue kernel command. Error Code=%d\n", err);
		exit(1);
	}
	// wait for the command to finish
	clFinish(command_queueGPU_h);


	err = clEnqueueReadBuffer( command_queueCPU_h, d_tex_CPU, CL_TRUE, 0, tamCPU, tex[0] , 0, NULL, NULL );
	errNDRANge(err);
	clFinish(command_queueCPU_h);
	
	err=clEnqueueReadBuffer( command_queueGPU_h, d_tex_GPU, CL_TRUE, 0, tamGPU, tex[0] + tamCPU , 0, NULL, NULL );
	//errNDRANge(err);		
	clFinish(command_queueGPU_h);
	
	clReleaseMemObject(d_tex_CPU);  
	clReleaseMemObject(d_tex_GPU);    
    clReleaseProgram(programCPU_h);
    clReleaseProgram(programGPU_h);
    clReleaseKernel(kernelGPU_h);
    clReleaseKernel(kernelCPU_h);
    clReleaseCommandQueue(command_queueCPU_h);
    clReleaseCommandQueue(command_queueGPU_h);
    clReleaseContext(contextCPU_h);
    clReleaseContext(contextGPU_h);
	
	return (getMicroSeconds() - t0);



}
void checkClCreateBuffer(char* variable,int err){

	if (err==CL_INVALID_CONTEXT){
		printf("CL_INVALID_CONTEXT for %s ",variable);
		exit(1); 
	}

	if (err==CL_INVALID_VALUE){
		printf("CL_INVALID_VALUE for %s ",variable);
		exit(1); 
	}
	if (err==CL_INVALID_BUFFER_SIZE){
		printf("CL_INVALID_BUFFER_SIZE for %s ",variable);
		exit(1); 
	}
	if (err==CL_INVALID_HOST_PTR){
		printf("CL_INVALID_HOST_PTR for %s ",variable);
		exit(1); 
	}
	if (err==CL_MEM_OBJECT_ALLOCATION_FAILURE){
		printf("CL_MEM_OBJECT_ALLOCATION_FAILURE for %s ",variable);
		exit(1); 
	}
	if (err==CL_OUT_OF_HOST_MEMORY){
		printf("CL_OUT_OF_HOST_MEMORY for %s ",variable); 
		exit(1); 
	}
	
}




double calc_mandel_opencl(cl_context *context,cl_program * program,cl_kernel *kernel,cl_command_queue *command_queue,cl_device_id *device_id,int cpu) {
	int err;               // error code returned from OpenCL calls
	size_t global[2];               // global domain size

	
	
if (cpu==1){ //si es la gpu dedicada, destruyo todo y vuelvo a inicializar
	//esto se debe a que la GPU de mi casa no tiene casi nada de memoria y muestra cl_out of resources
	    clReleaseProgram(*program);
   		clReleaseKernel(*kernel);
    	clReleaseCommandQueue(*command_queue);
        clReleaseContext(*context);
        inicializaGPU(context,command_queue,program,kernel,1,device_id);
     
    
}



	cl_mem d_tex;
	double t0;
	t0 = getMicroSeconds();	

	err = output_device_info(*device_id);
	d_tex = clCreateBuffer(*context,  CL_MEM_READ_WRITE, sizeTex, NULL, &err);
	//printf ("clCreateBuffer %s \n",err_code(err));

	
	
	if(clEnqueueWriteBuffer(*command_queue, d_tex, CL_TRUE, 0, sizeTex,tex[0], 0, NULL, NULL)!=CL_SUCCESS){
		printf("Problemas al transferir Tex!\n");
		exit(1);
	}

	
	
	

	

	if (clSetKernelArg(*kernel, 0, sizeof(cl_int), &height) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(*kernel, 1, sizeof(cl_int), &width)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 1\n");
	if (clSetKernelArg(*kernel, 2, sizeof(cl_double), &scale)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 2\n");
	if (clSetKernelArg(*kernel, 3, sizeof(cl_double), &cx)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 3\n");
	if (clSetKernelArg(*kernel, 4, sizeof(cl_double), &cy)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 4\n");
	if (clSetKernelArg(*kernel, 5, sizeof(cl_int), &max_iter)!= CL_SUCCESS)		
		printf("a fallado el setKernelArg 5\n");		
	if (clSetKernelArg(*kernel, 6, sizeof(cl_mem), &d_tex)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 6\n");
	if (clSetKernelArg(*kernel, 7, sizeof(cl_int), &invert)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 7\n");
	if (clSetKernelArg(*kernel, 8, sizeof(cl_int), &saturation)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 8\n");
	if (clSetKernelArg(*kernel, 9, sizeof(cl_int), &color_rotate)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 9\n");
	


	// set the global work dimension size
	global[0] = height;
	global[1] = width;

	// Enqueue the kernel object with 
	// Dimension size = 2, 
	// global worksize = global, 
	// local worksize = NULL - let OpenCL runtime determine
	// No event wait list	
	
	err = clEnqueueNDRangeKernel(*command_queue, *kernel, 2, NULL, global, NULL,0, NULL, NULL);
	

	if (err != CL_SUCCESS) {
		printf("Unable to enqueue kernel command. Error Code=%d\n", err);
		exit(1);
	}

	

	// wait for the command to finish
	clFinish(*command_queue);
	
	clEnqueueReadBuffer( *command_queue, d_tex, CL_TRUE, 0, sizeTex, tex[0] , 0, NULL, NULL );
	 
		
	clFinish(*command_queue);
	
	clReleaseMemObject(d_tex);   
   /*clReleaseProgram(*program);
    clReleaseKernel(*kernel);
    clReleaseCommandQueue(*command_queue);
    clReleaseContext(*context);*/
	
	return (getMicroSeconds() - t0);
}




void render() {
	double x = (double) width / tex_w, y = (double) height / tex_h;
	glClear(GL_COLOR_BUFFER_BIT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2i(0, 0);
	glTexCoord2f(x, 0);
	glVertex2i(width, 0);
	glTexCoord2f(x, y);
	glVertex2i(width, height);
	glTexCoord2f(0, y);
	glVertex2i(0, height);
	glEnd();
	glFlush();
	glFinish();
}


void screen_shot() {
	char fn[100];
	int i;
	int shots = 1;
	sprintf(fn, "screen%03d.ppm", shots++);
	FILE *fp = fopen(fn, "w");
	fprintf(fp, "P6\n%d %d\n255\n", width, height);
	for (i = height - 1; i >= 0; i--)
		fwrite(tex[i], 1, width * 3, fp);
	fclose(fp);
	printf("%s written\n", fn);
}

void keypress(unsigned char key, int x, int y) {
	switch (key) {
	case 'q':
		glFinish();
		glutDestroyWindow(gwin);
		clReleaseProgram(programCPU);
   		clReleaseKernel(kernelCPU);
    	clReleaseCommandQueue(command_queueCPU);
        clReleaseContext(contextCPU);

        clReleaseProgram(programGPU);
   		clReleaseKernel(kernelGPU);
    	clReleaseCommandQueue(command_queueGPU);
        clReleaseContext(contextGPU);
		return;
	case 27:
		scale = 1. / 256;
		cx = -.6;
		cy = 0;
		break;

	case 'r':
		color_rotate = (color_rotate + 1) % 6;
		break;

	case '>':
	case '.':
		max_iter += 64;
		if (max_iter > 1 << 15)
			max_iter = 1 << 15;
		printf("max iter: %d\n", max_iter);
		break;

	case '<':
	case ',':
		max_iter -= 64;
		if (max_iter < 64)
			max_iter = 64;
		printf("max iter: %d\n", max_iter);
		break;

	case 'm':
		saturation = 1 - saturation;
		break;

	case 'i':
		screen_shot();
		return;
	case 'z':
		max_iter = 4096;
		break;
	case 'x':
		max_iter = 128;
		break;
	case 's':
		run_mode = RUN_SERIAL;
		break;
	case 'c':
		run_mode = RUN_OPENCL_CPU;		
		break;
	case 'g':
		run_mode = RUN_OPENCL_GPU;
		break;
	case 'h':
		run_mode = RUN_OPENCL_HETEROGENEOUS;
		break;
	case ' ':
		invert = !invert;
	}
	set_texture();
}

void hsv_to_rgb(int hue, int min, int max, rgb_t *p) {
	if (min == max)
		max = min + 1;
	if (invert)
		hue = max - (hue - min);
	if (!saturation) {
		p->r = p->g = p->b = 255 * (max - hue) / (max - min);
		return;
	}
	double h = fmod(color_rotate + 1e-4 + 4.0 * (hue - min) / (max - min), 6);
#	define VAL 255
	double c = VAL * saturation;
	double X = c * (1 - fabs(fmod(h, 2) - 1));

	p->r = p->g = p->b = 0;

	switch ((int) h) {
	case 0:
		p->r = c;
		p->g = X;
		return;
	case 1:
		p->r = X;
		p->g = c;
		return;
	case 2:
		p->g = c;
		p->b = X;
		return;
	case 3:
		p->g = X;
		p->b = c;
		return;
	case 4:
		p->r = X;
		p->b = c;
		return;
	default:
		p->r = c;
		p->b = X;
	}
}


double calc_mandel() {
	int i, j, iter, min, max,contadorBreak;
	rgb_t *pixel;
	double x, y, zx, zy, zx2, zy2;
	double t0;

	t0 = getMicroSeconds();
	min = max_iter;
	max = 0;


	for (i = 0; i < height; i++) {
					
			pixel = tex[i];				
			y = (i - height / 2) * scale + cy;
			for (j = 0; j < width; j++) {
				x = (j - width / 2) * scale + cx;
				
				pixel = pixel +1;					
				zx = zy = zx2 = zy2 = 0;
				for (iter = 0; iter < max_iter; iter++) {
					zy = 2 * zx * zy + y;
					zx = zx2 - zy2 + x;
					zx2 = zx * zx;
					zy2 = zy * zy;
					
					if (zx2 + zy2 > max_iter)					
						break;
					
				}
				
				if (iter < min)					
					min = iter;
			
				
				if (iter > max)								
					max = iter;			
				
				*(unsigned short *) pixel = iter;				
				
			}
		}
		
	for (i = 0; i < height; i++){
		pixel = tex[i];
			for (j = 0 ; j < width; j++){
				pixel = pixel +1;			
				hsv_to_rgb(*(unsigned short*) pixel, 0, max_iter, pixel);
				
				//hsv_to_rgb(3, min, max, pixel);
			}
	}
	//printf ("Fin calc_mandel() Valor de scale = %d cx = %d, cy = %d, height = %d, width = %d \n",scale,cx,cy,height,width);

	return (getMicroSeconds() - t0);
}

void alloc_tex() {
	int i, ow = tex_w, oh = tex_h;

	for (tex_w = 1; tex_w < width; tex_w <<= 1)
		;
	for (tex_h = 1; tex_h < height; tex_h  <<= 1) 
		;

	if (tex_h != oh || tex_w != ow){
		tex = realloc(tex, tex_h * tex_w * 3 + tex_h * sizeof(rgb_t*));
		sizeTex = tex_h * tex_w * 3;// + tex_h * sizeof(rgb_t*);
		
	}
	for (tex[0] = (rgb_t *) (tex + tex_h), i = 1; i < tex_h; i++)
		tex[i] = tex[i - 1] + tex_w;
}

void set_texture() {
	double t;
	char title[128];
	//printf ("set_texture()  antes del alloc_tex() dentro del case Valor de scale = %d cx = %d, cy = %d, height = %d, width = %d \n",scale,cx,cy,height,width);		
	alloc_tex();
	switch (run_mode) {
	case RUN_SERIAL:
		t = calc_mandel();
		break;
	case RUN_OPENCL_CPU:			
		t = calc_mandel_opencl(&contextCPU,&programCPU,&kernelCPU,&command_queueCPU,&device_idCPU,0);	 	
		break;
	case RUN_OPENCL_GPU:
		t = calc_mandel_opencl(&contextGPU,&programGPU,&kernelGPU,&command_queueGPU,&device_idGPU,1);	
		break;
	case RUN_OPENCL_HETEROGENEOUS:
		t = calc_mandel_opencl_Heterogeneous();	
		break;
	};

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, tex_w, tex_h, 0, GL_RGB, GL_UNSIGNED_BYTE,
			tex[0]);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	render();

	sprintf(title, "Mandelbrot: %5.2f fps (%ix%i)", 1000000 / t, width, height);
	glutSetWindowTitle(title);
}

void mouseclick(int button, int state, int x, int y) {
	if (state != GLUT_UP)
		return;

	cx += (x - width / 2) * scale;
	cy -= (y - height / 2) * scale; 

	switch (button) {
	case GLUT_LEFT_BUTTON: /* zoom in */
		if (scale > fabs(x) * 1e-16 && scale > fabs(y) * 1e-16)
			scale /= 2;
		break;
	case GLUT_RIGHT_BUTTON: /* zoom out */
		scale *= 2;
		break;
		/* any other button recenters */
	}
	set_texture();
}

void resize(int w, int h) {
	//printf("resize %d %d\n", w, h);
	width = w;
	height = h;

	glViewport(0, 0, w, h);
	glOrtho(0, w, 0, h, -1, 1);

	set_texture();
}

void init_gfx(int *c, char **v) {
	glutInit(c, v);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutDisplayFunc(render);

	gwin = glutCreateWindow("Mandelbrot");

	glutKeyboardFunc(keypress);
	glutMouseFunc(mouseclick);
	glutReshapeFunc(resize);
	glGenTextures(1, &texture);
	set_texture();	

	
	
	
	


}

int main(int c, char **v) {
	//printf("Voy bien hasta aqui en el main %i y %s",c,v);
	inicializaGPU(&contextCPU, &command_queueCPU,&programCPU,&kernelCPU,0,&device_idCPU);
	inicializaGPU(&contextGPU, &command_queueGPU,&programGPU,&kernelGPU,1,&device_idGPU);
	init_gfx(&c, v); 
	
	printf(
			"keys:\n\tr: color rotation\n\tm: monochrome\n\ti: screen shot\n\t"
					"s: serial code\n\tc: OpenCL CPU\n\tg: OpenCL GPU\n\th: OpenCL Heterogeneo\n\t"
					"<, >: decrease/increase max iteration\n\tq: quit\n\tmouse buttons to zoom\n");

	glutMainLoop();
   //Libero la GPU
	
	return 0;
}



