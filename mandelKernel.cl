#pragma OPENCL EXTENSION cl_khr_fp64 : enable

typedef struct {
	unsigned char r, g, b;
} rgb_t;

void hsv_to_rgb(int hue, int min, int max, rgb_t *p,int invert, int saturation,int color_rotate) {
	if (min == max)
		max = min + 1;
	if (invert)
		hue = max - (hue - min);
	if (!saturation) {
		p->r = p->g = p->b = 255 * (max - hue) / (max - min);
		return;
	}
	double h = fmod(color_rotate + 1e-4 + 4.0 * (hue - min) / (max - min), 6);
#	define VAL 255
	double c = VAL * saturation;
	double X = c * (1 - fabs(fmod(h, 2) - 1));

	p->r = p->g = p->b = 0;

	switch ((int) h) {
	case 0:
		p->r = c;
		p->g = X;
		return;
	case 1:
		p->r = X;
		p->g = c;
		return;
	case 2:
		p->g = c;
		p->b = X;
		return;
	case 3:
		p->g = X;
		p->b = c;
		return;
	case 4:
		p->r = X;
		p->b = c;
		return;
	default:
		p->r = c;
		p->b = X;
	}
}


__kernel
void mandel(const  int height,const int width,const double scale, const double cx, const double cy,const int max_iter,__global char *tex,const int invert,const int saturation,const int color_rotate)

{
	int  ii,jj,iter,min,max;	
	double x, y, zx, zy, zx2, zy2;
	
	min = max_iter;
	max = 0;
	rgb_t *pixel;	

	int i = get_global_id(0);
	int j = get_global_id(1);

	//printf("Empezando el calculo en CPU \n");

	if (i<height){
					  	
			pixel = tex+i;							
			y = (i - height / 2) * scale + cy; 		
			if (j<width){
							
				x = (j - width / 2) * scale + cx;										
				pixel = pixel+1;//las 3 posiciones del RGB						
				zx = zy = zx2 = zy2 = 0;
				for (iter = 0; iter < max_iter; iter++) {	
							
					zy = 2 * zx * zy + y;						
					zx = zx2 - zy2 + x;						
					zx2 = zx * zx;					
					zy2 = zy * zy;							
					if (zx2 + zy2 > max_iter)											
						break;
					
				}	
				
				if (iter < min)					
					min = iter;			
				
				if (iter > max)		
					max = iter;			
								
				*(unsigned short *) pixel = iter;		

			}

	}
	barrier(CLK_GLOBAL_MEM_FENCE);	
	if (i<height){
		pixel = tex + i;
		if (j<width){
			pixel = pixel+1; 
			hsv_to_rgb(*(unsigned short*) pixel, 0,max_iter, pixel,invert,saturation,color_rotate);
		}
	}
			
}
